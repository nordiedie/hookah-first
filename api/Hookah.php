<?php

include_once 'DatabaseConnector.php';

class Hookah extends DatabaseConnector {

	public $list = [];
	public $limit = 5;

	public function getAll($page)
	{
		$offset = $page * $this->limit - $this->limit; 

		$result = $this->query("
			SELECT 
				`id`,
				`name`
			FROM
				hookahs
			WHERE 
				`active`=1
			LIMIT ".$this->limit." OFFSET $offset;
		");

		$values = [];
		while($row = $result->fetch_assoc()) {
        	$values[] = $row;
    	}

    	$this->list = $values;
    	return $values;
	}

	public function getPagesCount()
	{
		$result = $this->query("
			SELECT
				COUNT(*) AS 'cnt'
			FROM
				hookahs
			WHERE
				`active`=1;
		");

		$result = $result->fetch_assoc()['cnt'];
		$result = ceil($result / $this->limit);

		return $result;
	}

	public function getOne($id)
	{
		$id = $this->real_escape_string($id);

		$result = $this->query("
			SELECT
				*
			FROM 
				hookahs
			WHERE 
				`id`=$id AND 
				`active`=1
			LIMIT 1;
		");

		$this->list = $result->fetch_assoc();

		return $this->list;
	}

	public function add($name, $description, int $length, int $diameter) 
	{
		$name = $this->real_escape_string($name);
		$description = $this->real_escape_string($description);
		$length = $this->real_escape_string($length);
		$diameter = $this->real_escape_string($diameter);

		$isAdded = $this->query("
			INSERT INTO 
				hookahs
					(name, description, length, diameter)
				VALUES 
					('$name', '$description', $length, $diameter)
		");

		return $isAdded;
	}

	public function delete($id)
	{
		$id = $this->real_escape_string($id);

		$isDeleted = $this->query("
			UPDATE 
				hookahs
			SET
				`active`=0
			WHERE
				`id`=$id
		");

		return $isDeleted;
	}
}
