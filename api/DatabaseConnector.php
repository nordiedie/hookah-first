<?php 

abstract class DatabaseConnector extends MySQLi 
{
	public function __construct()
	{
		$host = '127.0.0.1';
		$user = 'root';
		$password = '';
		$database = 'hookahs';

		if (mysqli_connect_error()) {
			header('HTTP/1.1 500 Connection Error (' . mysqli_connect_errno() . ') '. mysqli_connect_error());
			echo 'Connection Error (' . mysqli_connect_errno() . ') '. mysqli_connect_error();
			die;
		}

		return parent::__construct($host, $user, $password, $database);
	}
}