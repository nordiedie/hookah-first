<?php 

include_once 'Hookah.php';

$page = $_GET['p'] ?? 1;

$hookah = new Hookah;

$list = $hookah->getAll($page);

$toShow = [];

foreach ($list as $value) {
	$toShow[] = [
		'id' => $value['id'],
		'name' => $value['name'],
	];
}

echo json_encode($toShow);