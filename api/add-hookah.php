<?php

$name = $_POST['name'];
$description = $_POST['description'];
$length = $_POST['length'];
$diameter = $_POST['diameter'];

include_once 'Hookah.php';

$hookah = new Hookah;

$isAdded = $hookah->add($name, $description, (int)$length, (int)$diameter);

if(!$isAdded) {
	header('HTTP/1.1 400 Hookah cannot be added');
	echo "Error adding a hookah";
	die;
}

return json_encode('success');
