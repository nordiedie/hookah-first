<?php

$id = $_GET['id'];

include_once 'Hookah.php';

$hookah = new Hookah;

$isDeleted = $hookah->delete($id);

if(!$isDeleted){
	header('HTTP/1.1 400 Hookah cannot be deleted');
	echo "Hookah cannot be deleted";
	die;
}

return json_encode('Deleted succesfully');
