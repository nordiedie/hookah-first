<?php 

$gotId = @$_GET['id'];

if (!isset($gotId)) {
	header('HTTP/1.1 404 No ID provided');
	echo "No Hookah ID";
	die;
}

include_once 'Hookah.php';

$hookah = new Hookah;

$list = $hookah->getOne($gotId);

if(!$list || count($list) < 1) {
	header('HTTP/1.1 404 Hookah not found');
	echo "There is no Hookah ID: ". $gotId;
	die;
}

echo json_encode($list);