let page = 1;

$(document).ready(function(){
	getHookahsList();
	renderPagination();
});

function getHookahsList() {
	$.ajax({
		url : 'api/all-hookahs.php',
		data: {p: page},
		success: function (data) {
			let hookahs = JSON.parse(data);
			console.log(hookahs);

			$.ajax({
				url: 'templates/hookah-buttons.html',
				success: function (hookahButtonsTemplate) {
					let template = Handlebars.compile(hookahButtonsTemplate);
					let html     = template({hookahs: hookahs});

					$('.place-for-buttons').html(html);
				}
			});

		}
	});
}

function renderPagination() {
	$.ajax({
		url: 'templates/pagination.html',
		success: function (pagesTemplate) {
			let template = Handlebars.compile(pagesTemplate);
			$.ajax({
				url: 'api/count-hookahs.php',
				success: function (pages) {
					pages = JSON.parse(pages);
					let html = template({
						pages: pages,
						previousPage: page-1,
						currentPage: page,
						nextPage: parseInt(page)+1,
						isFirstPage: parseInt(page) === 1,
						isLastPage: parseInt(page) === pages.length
					});
					$('.place-for-pagination').html(html);
				}
			});
		}
	});
}

$(document).on('click', '.get-hookah', function () {

	let hookahId = this.dataset.id;
	$.ajax({
		url: 'api/single-hookah.php',
		data: {id: hookahId},
		success: function (data) {
			let hookah  = JSON.parse(data);
			console.log(hookah);

			$.ajax({
				url: 'templates/card-hookah.html',
				success: function (cardHookahTemplate) {
					let template = Handlebars.compile(cardHookahTemplate);
					let html = template({hookah: hookah});

					$('.place-for-hookah').html(html);
				}
			});

		},
		error: function (xhr, status, error) {
			let errorText = 'Error: '+ xhr.status +' '+ error;

			$.ajax({
				url: 'templates/error-hookah.html',
				success: function (errorHookahTemplate) {
					let template = Handlebars.compile(errorHookahTemplate);
					let html = template({errorText: errorText});

					$('.place-for-hookah').html(html);
				}
			});
		}
	});

});

$(document).on('click', '.delete-hookah', function () {
  	let id = this.dataset.id;

  	$.ajax({
		url: 'api/delete-hookah.php',
		data: {id: id},
		success: function () {
			getHookahsList();
		}
	})
});

$(document).on('click', '.close', function () {
	$(this).parent().parent().fadeOut('slow');
});

$(document).on('click', '.create-hookah', function () {

	$.ajax({
		url: 'templates/create-hookah-form.html',
		success: function (formTemplate){
			let template = Handlebars.compile(formTemplate);
			let html = template();

			let placeForHookah = $('.place-for-hookah');
			placeForHookah.show();
			placeForHookah.html(html);
		}
	});
});

$(document).on('click', '.page-link', function () {
	page = this.dataset.page;
	getHookahsList();
	renderPagination();
});

$(document).on('submit', '.form-for-new-hookah', function (event) {
	event.preventDefault();

	let formData = new FormData(this);
	let dataToSend = {
		name: formData.get('name'),
		description: formData.get('description'),
		length: formData.get('length'),
		diameter: formData.get('diameter')
	};

	console.dir(dataToSend);

	$.ajax ({
		url: 'api/add-hookah.php',
		method: 'POST',
		data: dataToSend,
		success: function(){
			getHookahsList();
		}
	})

});




